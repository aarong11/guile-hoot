;;; R7RS (scheme file) library
;;; Copyright (C) 2024 Igalia, S.L.
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; R7RS (scheme file) implementation
;;;
;;; Code:

(library (scheme file)
  (export open-binary-input-file
          open-binary-output-file
          call-with-input-file
          call-with-output-file
          delete-file
          file-exists?
          open-input-file
          open-output-file
          with-input-from-file
          with-output-to-file)
  (import (scheme base)
    (only (hoot exceptions) make-unimplemented-error))

  (define (open-binary-input-file filename)
    (raise (make-unimplemented-error 'open-binary-input-file)))
  (define (open-binary-output-file filename)
    (raise (make-unimplemented-error 'open-binary-output-file)))
  (define (call-with-input-file filename proc)
    (raise (make-unimplemented-error 'call-with-input-file)))
  (define (call-with-output-file filename proc)
    (raise (make-unimplemented-error 'call-with-output-file)))
  (define (delete-file filename)
    (raise (make-unimplemented-error 'delete-file)))
  (define (file-exists? filename)
    (raise (make-unimplemented-error 'file-exists?)))
  (define (open-input-file filename)
    (raise (make-unimplemented-error 'open-input-file)))
  (define (open-output-file filename)
    (raise (make-unimplemented-error 'open-output-file)))
  (define (with-input-from-file filename thunk)
    (let ((p (open-input-file filename)))
      (parameterize ((current-input-port p))
        (call-with-values thunk
          (lambda vals
            (close-port p)
            (apply values vals))))))
  (define (with-output-to-file filename thunk)
    (let ((p (open-output-file filename)))
      (parameterize ((current-output-port p))
        (call-with-values thunk
          (lambda vals
            (close-port p)
            (apply values vals)))))))
