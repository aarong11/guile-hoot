;;; Library-group expander
;;; Copyright (C) 2024 Igalia, S.L.
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Expander for `library-group` form.
;;;
;;; Code:

(define-module (hoot library-group)
  #:use-module (ice-9 match)
  #:use-module (language tree-il)
  #:use-module (language tree-il primitives)
  #:use-module ((srfi srfi-1) #:select (append-map partition))
  #:use-module (srfi srfi-9)
  #:use-module ((system syntax internal) #:select (syntax? syntax-sourcev))
  #:use-module (system base target)
  #:export (expand-library-group))

(define-syntax-rule (with-hoot-target . body)
  (with-target "wasm32-unknown-hoot"
    (lambda ()
      (parameterize ((target-runtime 'hoot))
        . body))))

(define-record-type <import>
  (make-import modname exported-name imported-name)
  import?
  (modname import-modname)
  (exported-name exported-name)
  (imported-name imported-name))

(define-record-type <lexical>
  (make-lexical sym)
  lexical?
  (sym lexical-sym))

(define-record-type <primitive>
  (make-primitive name)
  primitive?
  (name primitive-name))

(define-record-type <expand-time-value>
  (make-expand-time-value)
  expand-time-value?)

;; <value> := <lexical>
;;          | <primitive>
;;          | <expand-time-value>
(define-record-type <module-definitions>
  (make-module-definitions private public)
  module-definitions?
  ;; Hash table of symbol -> <value>.
  (private module-private-definitions)
  ;; Hash table of symbol -> <value>.
  (public module-public-definitions))

(define-record-type <definition>
  (make-definition name sym val)
  definition?
  (name definition-name)
  (sym definition-sym)
  (val definition-val))

(define-record-type <statement>
  (make-statement exp)
  statement?
  (exp statement-exp))

;; FIXME: Get this exported from (language tree-il primitives).
(define (primitive-for-variable box)
  (hashq-ref (@@ (language tree-il primitives) *interesting-primitive-vars*)
             box))

(define (syntax-filename form)
  (and (syntax? form)
       (match (assq-ref (or (syntax-source form) '()) 'filename)
         ((? string? f) f)
         (_ #f))))

(define (expand-library mod form)
  "Expand the syntax object @var{form} in the module @var{mod}.

The term will be expanded twice: once to create the expand-time module,
which will then be evaluated directly, and once to residualize a Tree-IL
term for the compilation unit.

Syntax transformers (macros) will be evaluated at expansion-time, and
not residualized into the compilation unit."
  (save-module-excursion
   (lambda ()
     (set-current-module mod)
     (primitive-eval (macroexpand form 'e '(expand eval)))
     (with-hoot-target (macroexpand form 'c '())))))

(define (expand-program mod form)
  "Expand the syntax object @var{form} in the module @var{mod}.

Syntax transformers (macros) will be evaluated at expansion-time, and
not residualized into the compilation unit."
  (save-module-excursion
   (lambda ()
     (set-current-module mod)
     (with-hoot-target (macroexpand form 'c '(expand))))))

(define* (expand-library-group exp #:key
                               (import-abi? #f)
                               (export-abi? #t)
                               (primitives '(hoot primitives)))
  "Take a @code{library-group} form and expand it to a big @code{letrec*}.
The libraries in the group are expanded one-by-one.  Expanding a library
residualises a Tree-IL AST node as part of the compilation unit, and
additionally populates a compile-time host module with definitions.  If
expanding a module needs compile-time values from another module, it
uses the bindings in the host module.

All definitions and expressions in the expanded libraries are then
rewritten to be part of a big @code{letrec*}, and top-level and module
references in those definitions and expressions are rewritten to use
lexical references.

The final program in the @code{library-group} is given the same
treatment, except that its final expression (if any) is evaluated in
tail position."
  (define (name-matches? stx sym)
    (eq? (syntax->datum stx) sym))
  (define-syntax-rule (symbolic-match? name)
    (name-matches? #'name 'name))
  (define (id? x) (symbol? x))
  (define (name-component? x) (id? x))
  (define (version-component? x) (and (exact-integer? x) (not (negative? x))))

  (define parse-name+version
    (match-lambda
      (((? name-component? name) ... ((? version-component? version) ...))
       (values name version))
      (((? name-component? name) ...)
       (values name '()))))

  (define (parse-exports exports)
    ;; -> ((local . public) ...)
    (append-map (match-lambda
                  ((? id? id) `((,id . ,id)))
                  (('rename ((? id? from) (? id? to)) ...)
                   (map cons from to)))
                exports))

  ;; A mapping from module,name,public? tuple to <binding> record, for
  ;; all modules in the library group.
  (define module-definitions (make-hash-table))
  (define (add-module-definitions! modname)
    (when (hash-ref module-definitions modname)
      (error "duplicate module" modname))
    (define defs
      (make-module-definitions (make-hash-table) (make-hash-table)))
    (hash-set! module-definitions modname defs)
    defs)
  (define (lookup-module-definitions modname)
    (or (hash-ref module-definitions modname)
        (error "unknown module" modname)))

  (define (add-definition! defs name public? value)
    (match defs
      (($ <module-definitions> private public)
       (let ((t (if public? public private)))
         (when (hashq-ref t name)
           (error "duplicate definition" name))
         (hashq-set! t name value)))))
  (define (lookup-definition defs name public?)
    (match defs
      (($ <module-definitions> private public)
       (hashq-ref (if public? public private) name))))

  ;; Add definitions from primitive module.
  (when primitives
    (let ((defs (add-module-definitions! primitives)))
      (module-for-each
       (lambda (name box)
         (add-definition! defs name #t
                          (match (primitive-for-variable box)
                            (#f (make-expand-time-value))
                            (name (make-primitive name)))))
       (resolve-interface primitives))))

  (define (parse-imports import-sets trusted?)
    (define parse-import-set
      (match-lambda
        ((head . tail)
         (match head
           ('only
            (match tail
              ((iset (? id? select) ...)
               (filter (match-lambda
                         (($ <import> mod-name exported imported)
                          (memq imported select)))
                       (parse-import-set iset)))))
           ('except
            (match tail
              ((iset (? id? hide) ...)
               (filter (match-lambda
                         (($ <import> mod-name exported imported)
                          (not (memq imported hide))))
                       (parse-import-set iset)))))
           ('prefix
            (match tail
              ((iset (? id? prefix))
               (map (match-lambda
                      (($ <import> mod-name exported imported)
                       (let ((renamed (symbol-append prefix imported)))
                         (make-import mod-name exported renamed))))
                    (parse-import-set iset)))))
           ('rename
            (match tail
              ((iset ((? id? from) (? id? to)) ...)
               (let ((renamings (map cons from to)))
                 (map (match-lambda
                        (($ <import> mod-name exported imported)
                         (define renamed
                           (or (assq-ref renamings imported) imported))
                         (make-import mod-name exported renamed)))
                      (parse-import-set iset))))))
           ('library
               (match tail
                 ((((? name-component? modname) ... (and version (_ ...))))
                  (unless (null? version)
                    (error "version references unsupported"))
                  (when (equal? modname primitives)
                    (unless trusted?
                      (error "untrusted module cannot import primitives")))
                  (let ((exports (module-public-definitions
                                  (lookup-module-definitions modname))))
                    (define (id<? a b)
                      (string<? (symbol->string a) (symbol->string b)))
                    (define (import<? a b)
                      (id<? (exported-name a) (exported-name b)))
                    (sort (hash-map->list (lambda (name binding)
                                            (make-import modname name name))
                                          exports)
                          import<?)))
                 ((((? name-component? modname) ...))
                  (parse-import-set `(library (,@modname ()))))))
           (_
            (parse-import-set `(library (,head . ,tail))))))))
    (append-map (match-lambda
                  ;; Strip level.
                  (('for iset level ...) (parse-import-set iset))
                  (iset                  (parse-import-set iset)))
                import-sets))

  ;; Because each invocation of expand-library-group gets its own
  ;; namespace, we don't have to deal with lingering definitions from
  ;; any previous expansion; all modules defined by this compilation
  ;; unit are fresh.  This also allows expansion to happen in parallel.
  (define namespace (gensym "%library-group"))

  (define (host-modname? modname)
    (match modname
      (() #f)
      ((head . tail)
       (not (eq? namespace head)))))
  (define (annotate-modname modname)
    (if (equal? modname primitives)
        modname
        (cons namespace modname)))
  (define (strip-modname modname)
    (match modname
      (((? (lambda (x) (eq? x namespace))) . modname) modname)
      (_
       (unless (equal? modname primitives)
         (error "unexpected modname" modname))
       modname)))

  (define (make-expand-time-module modname filename version imports exports)
    "Create the host module in which to store compile-time library
definitions.  The module may import other host libraries."
    (define imports-by-module (make-hash-table))
    (define (add-import! modname exported imported)
      (define tail (hash-ref imports-by-module modname '()))
      (define entry (cons exported imported))
      (hash-set! imports-by-module modname (cons entry tail)))
    (for-each (match-lambda
                (($ <import> modname exported imported)
                 (add-import! modname exported imported)))
              imports)
    (define (id<? a b)
      (string<? (symbol->string a) (symbol->string b)))
    (define (modname<? a b)
      (match a
        (() #t)
        ((a . a*) (match b
                    (() #f)
                    ((b  . b*) (and (id<? a b) (modname<? a* b*)))))))
    (define module-import-decls
      (sort (hash-map->list (lambda (modname entries)
                              (list (annotate-modname modname)
                                    #:select
                                    (sort entries
                                          (lambda (a b)
                                            (id<? (car a) (car b))))))
                            imports-by-module)
            (lambda (a b)
              (modname<? (car a) (car b)))))
    (define-values (module-export-decls module-re-export-decls)
      (let ()
        (define imports-by-name (make-hash-table))
        (for-each (match-lambda
                    ((and import ($ <import> _ _ imported))
                     (match (hashq-ref imports-by-name imported)
                       (#f (hashq-set! imports-by-name imported import))
                       (existing
                        (error "duplicate imports" existing import)))))
                  imports)
        (partition (match-lambda
                     ((local . public) (not (hashq-ref imports-by-name local))))
                   exports)))
    (define-module* (annotate-modname modname)
      #:filename filename
      #:pure #t
      #:version version
      #:imports module-import-decls
      #:exports module-export-decls
      #:re-exports module-re-export-decls
      #:declarative? #t))

  (define (tree-il->reversed-bindings exp modname imports exports bindings)
    "Given the expanded library @var{exp}, as a Tree-IL node, transform it to
a sequence of definitions and expressions, as @code{<binding>} nodes.
Rewrite references to other top-level bindings to refer to primitive or
lexical definitions.  Append those @code{<binding>} nodes to
@var{bindings}, in reverse order."
    ;; Make defs for module.
    (define defs (add-module-definitions! modname))

    (define (has-expand-time-value? name)
      (module-variable (resolve-module (annotate-modname modname)) name))

    ;; Add definitions for imports.
    (for-each (match-lambda
                (($ <import> imod exported imported)
                 (match (lookup-definition (lookup-module-definitions imod)
                                           exported #t)
                   (#f (error "unknown import?" imod exported))
                   (value (add-definition! defs imported #f value)))))
              imports)

    (define (tree-il-for-each f exp)
      (define fold (make-tree-il-folder))
      (fold exp (lambda (exp) (values)) f))

    ;; Prohibit set! to imports.  Check module on expanded toplevel defs
    ;; and uses.
    (tree-il-for-each (match-lambda
                        (($ <toplevel-define> src mod name val)
                         (unless (equal? (strip-modname mod) modname)
                           (error "unexpected mod" exp mod modname))
                         (values))
                        (($ <toplevel-ref> src mod name)
                         (unless (equal? (strip-modname mod) modname)
                           (error "unexpected mod" exp mod modname))
                         (values))
                        (($ <toplevel-set> src mod name val)
                         (unless (equal? (strip-modname mod) modname)
                           (error "unexpected mod" exp mod modname))
                         (when (lookup-definition defs name #f)
                           (error "set! to imported binding" src name))
                         (values))
                        (_ (values)))
                      exp)

    ;; Record local definitions and allocate lexicals for them.
    (tree-il-for-each (match-lambda
                        (($ <toplevel-define> src mod name exp)
                         (when (lookup-definition defs name #f)
                           (error "duplicate definition" modname name))
                         (add-definition! defs name #f (make-lexical (gensym "top")))
                         (values))
                        (_ (values)))
                      exp)

    ;; Check for unbound top-levels.
    (tree-il-for-each (match-lambda
                        (($ <toplevel-ref> src mod name)
                         (unless (lookup-definition defs name #f)
                           (error "unbound top-level" src name))
                         (values))
                        (($ <toplevel-set> src mod name val)
                         (unless (lookup-definition defs name #f)
                           (error "unbound top-level" src name))
                         (values))
                        (_ (values)))
                      exp)

    ;; Find local definitions for exports.
    (for-each (match-lambda
                ((local . exported)
                 (match (lookup-definition defs local #f)
                   (#f
                    ;; An export without a binding in the compilation
                    ;; unit.  Perhaps it is an expansion-time binding.
                    (unless (has-expand-time-value? local)
                      (error "missing definition for export"
                             modname local exported))
                    (let ((val (make-expand-time-value)))
                      (add-definition! defs local #f val)
                      (add-definition! defs exported #t val)))
                   (val (add-definition! defs exported #t val)))))
              exports)

    ;; Resolve references to local definitions and residualized
    ;; module-private definitions to lexical-ref or primitive-ref.
    (define (visit-expr exp)
      (post-order
       (lambda (exp)
         (match exp
           (($ <toplevel-ref> src mod name)
            (match (lookup-definition defs name #f)
              (($ <lexical> sym) (make-lexical-ref src name sym))
              (($ <primitive> name) (make-primitive-ref src name))
              (($ <expand-time-value>)
               (error "reference to expansion-time value in generated code"
                      src modname name))))
           (($ <toplevel-set> src mod name val)
            (match (lookup-definition defs name #f)
              (($ <lexical> sym) (make-lexical-set src name sym val))
              (($ <expand-time-value>)
               (error "reference to expansion-time value in generated code"
                      src modname name))))
           (($ <module-ref> src (? host-modname? mod) name #f)
            ;; A primitive reference introduced by a primitive syntax
            ;; expander.
            (match (primitive-for-variable
                    (module-variable (resolve-module mod) name))
              (#f (error "can't find name for primitive reference" mod name))
              (name (make-primitive-ref src name))))
           (($ <module-ref> src mod name public?)
            (let ((defs (lookup-module-definitions (strip-modname mod))))
              (match (lookup-definition defs name public?)
                (($ <lexical> sym) (make-lexical-ref src name sym))
                (($ <primitive> name) (make-primitive-ref src name))
                (($ <expand-time-value>)
                 (error "reference to expansion-time value in generated code"
                        src mod name)))))
           (($ <module-set> src mod name public? val)
            (let ((defs (lookup-module-definitions (strip-modname mod))))
              (match (lookup-definition defs name public?)
                (($ <lexical> sym) (make-lexical-set src name sym val))
                (($ <expand-time-value>)
                 (error "reference to expansion-time value in generated code"
                        src mod name)))))
           (($ <toplevel-define>)
            (error "unexpected nested toplevel define" exp))
           (($ <call> src ($ <primitive-ref> _ name) args)
            (expand-primcall (make-primcall src name args)))
           (_ exp)))
       exp))

    ;; Walk the chain of <seq> and <toplevel-define> to extract
    ;; definitions and statements.
    (define (visit-top-level exp bindings)
      (match exp
        (($ <toplevel-define> src mod name val)
         (match (lookup-definition defs name #f)
           (($ <lexical> sym)
            (cons (make-definition name sym (visit-expr val))
                  bindings))))

        (($ <seq> src head tail)
         (visit-top-level tail (visit-top-level head bindings)))

        ;; Could fold in let and letrec* bindings.  Dunno.
        (_ (cons (make-statement (visit-expr exp)) bindings))))

    (visit-top-level exp bindings))

  (define (r6rs-library->reversed-bindings form bindings trusted?)
    "Given the R6RS library @var{form}, as a syntax object, parse out the
imports and exports, create a compile-time module, and expand the body
of the library within that module.  Add the residual definitions and
expressions from the module to @var{bindings}, as in
@code{tree-il->reversed-bindings}."
    (syntax-case form ()
      ((library (name ...)
         (export export-spec ...)
         (import import-spec ...)
         body ...)
       (and (symbolic-match? library)
            (symbolic-match? export)
            (symbolic-match? import))
       (let ()
         (define filename (syntax-filename #'library))
         (define-values (modname version)
           (parse-name+version (syntax->datum #'(name ...))))
         (define exports
           (parse-exports (syntax->datum #'(export-spec ...))))
         (define imports
           (parse-imports (syntax->datum #'(import-spec ...)) trusted?))
         (define ctmod
           (make-expand-time-module modname filename version imports exports))
         (define wrapped )
         (define expanded (expand-library ctmod #'(begin body ...)))
         (tree-il->reversed-bindings expanded modname imports exports
                                     bindings)))))

  (define (program->reversed-bindings forms imports bindings)
    "Same as @code{r6rs-library->reversed-bindings}, but for a program.
@var{imports} is already parsed, as a list of @code{<import>}.  A new
module with a fresh name will be defined for the purposes of expanding "
    (define modname (list (gensym "library-group-program")))
    (define ctmod
      (make-expand-time-module modname (syntax-filename forms) '() imports '()))
    (define expanded (expand-program ctmod #`(begin . #,forms)))
    (tree-il->reversed-bindings expanded modname imports '() bindings))

  (define* (library-group->reversed-bindings forms bindings #:key (trusted? #f))
    "For each form in @var{forms}, which should be a list of syntax objects,
process any includes, then determine whether it is a library or a
program.  If it is a library, expand it and extract its bindings.  If it
is a program, parse its imports, expand the body, and stop processing.
The result is a reversed list of @code{<binding>}, suitable for
splatting into a @code{letrec*}."
    (syntax-case forms ()
      (() (cons (make-statement (make-void #f)) bindings))
      ((form . forms)
       (syntax-case #'form ()
         (#:untrusted
          (library-group->reversed-bindings #'forms bindings #:trusted? #f))
         ((library . _)
          (symbolic-match? library)
          (library-group->reversed-bindings
           #'forms
           (r6rs-library->reversed-bindings #'form bindings trusted?)
           #:trusted? trusted?))
         ((define-library . _)
          (symbolic-match? define-library)
          (error "R7RS libraries not yet supported"))
         ((include filename)
          (symbolic-match? include)
          (call-with-include-port
           (datum->syntax
            #f
            (canonicalize-path
             (or (%search-load-path (syntax->datum #'filename))
                 (syntax-violation 'include
                                   "file not found in path"
                                   #'form #'filename))))
           (lambda (p)
             (library-group->reversed-bindings
              (append (let lp ()
                        (match (read-syntax p)
                          ((? eof-object?) #'())
                          (x (cons x (lp)))))
                      #'forms)
              bindings #:trusted? trusted?))))
         ((import import-spec ...)
          (symbolic-match? import)
          (program->reversed-bindings
           #'forms
           (parse-imports (syntax->datum #'(import-spec ...)) trusted?)
           bindings))))))

  (syntax-case exp ()
    ((library-group form ...)
     (symbolic-match? library-group)
     (let ((src (and (syntax? #'library-group)
                     (syntax-sourcev #'library-group))))
       (match (library-group->reversed-bindings #'(form ...) '() #:trusted? #t)
         ((($ <statement> tail) . bindings)
          (let ((bindings (reverse bindings)))
            (make-letrec src
                         #t             ; in-order?
                         (map (match-lambda
                                (($ <definition> name sym val) name)
                                (($ <statement> exp) '_))
                              bindings)
                         (map (match-lambda
                                (($ <definition> name sym val) sym)
                                (($ <statement> exp) (gensym "_")))
                              bindings)
                         (map (match-lambda
                                (($ <definition> name sym val) val)
                                (($ <statement> exp)
                                 (if (void? exp)
                                     exp
                                     (make-seq #f exp (make-void #f)))))
                              bindings)
                         tail))))))
    (_
     ;; FIXME: Add some kind of standard libraries?
     (error "missing a library-group wrapper" exp))))
