;;; Exception constructors
;;; Copyright (C) 2024 Igalia, S.L.
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Constructors for common exceptions.
;;;
;;; Code:

(library (hoot exceptions)
  (export make-size-error
          make-index-error
          make-range-error
          make-start-offset-error
          make-end-offset-error
          make-type-error
          make-unimplemented-error
          make-assertion-error
          make-not-seekable-error
          make-runtime-error-with-message
          make-runtime-error-with-message+irritants
          make-match-error
          make-arity-error)
  (import (hoot primitives))

  (define-syntax-rule (define-exception-constructor (name arg ...) global)
    (define (name arg ...)
      ((%inline-wasm '(func (result (ref eq)) (global.get global))) arg ...)))

  (define-exception-constructor (make-size-error val max who)
    $make-size-error)
  (define-exception-constructor (make-index-error val size who)
    $make-index-error)
  (define-exception-constructor (make-range-error val min max who)
    $make-range-error)
  (define-exception-constructor (make-start-offset-error val size who)
    $make-start-offset-error)
  (define-exception-constructor (make-end-offset-error val size who)
    $make-end-offset-error)
  (define-exception-constructor (make-type-error val who what)
    $make-type-error)
  (define-exception-constructor (make-unimplemented-error who)
    $make-unimplemented-error)
  (define-exception-constructor (make-assertion-error expr who)
    $make-assertion-error)
  (define-exception-constructor (make-not-seekable-error port who)
    $make-not-seekable-error)
  (define-exception-constructor (make-runtime-error-with-message msg)
    $make-runtime-error-with-message)
  (define-exception-constructor (make-runtime-error-with-message+irritants msg irritants)
    $make-runtime-error-with-message+irritants)
  (define-exception-constructor (make-match-error v)
    $make-match-error)
  (define-exception-constructor (make-arity-error v who)
    $make-arity-error))
